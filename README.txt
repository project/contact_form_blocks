$Id

Overview
--------
The Contact form blocks module makes your site-wide contact forms 
available as Drupal Blocks.

Using this module you can show your contact forms on any place
where you can show a Drupal Block. For instance you may add a contact
form to the content of a page using the Block visibility settings. 


Requirements
------------
Drupal's contact module must be enabled.


Installation
------------
Get the module from http://drupal.org/project/contact_form_blocks
Copy the contact_form_blocks directory to the 
module directory of your site and enable it via 
Administer > Site building > Modules ("Other" section).


Usage
-----
Create at least one site-wide contact form category at 
Administer > Site building > Contact form.

Now you get for every contact form category one block at 
Administer > Site building > Blocks. Use the visibility 
settings at the block configuration to show the block on 
one page or some pages only or use other settings there.

If you don't want all Contact form categories to be shown 
in the site wide contact form you can switch some off at 
Administer > Site configuration > Contact blocks.


Author/Maintainer
-----------------
Bernhard Fürst <bernhard.fuerst@fuerstnet.de>
http://fuerstnet.de
